defmodule Hangman.MixProject do
  use Mix.Project

  def project do
    [
      app: :hangman,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Hangman.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:dictionary, github: "rawandrew/dictionary"},
      {:elixir_uuid, "~> 1.2"},
      {:plug_cowboy, "~> 2.0"},
      {:poison, "~> 4.0"},
      {:mock, "~> 0.3.0", only: :test}
    ]
  end
end
